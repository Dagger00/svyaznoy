DROP TABLE log;
CREATE TABLE log
(
  id      INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  number  VARCHAR(11)     NOT NULL,
  date    DATETIME        NOT NULL,
  status  INT             NOT NULL,
  message VARCHAR(255)
);