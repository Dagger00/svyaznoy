package com.getjavajob.training.svyaznoy.khaninev.sms.repository;

/**
 * Created by zenet on 19.09.2015.
 */
public class PageTO {
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
