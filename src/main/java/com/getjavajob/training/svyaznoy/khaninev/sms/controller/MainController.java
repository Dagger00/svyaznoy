package com.getjavajob.training.svyaznoy.khaninev.sms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by zenet on 18.09.2015.
 */
@Controller
@RequestMapping(value = "/*")
public class MainController {

    @RequestMapping(method = RequestMethod.GET)
    public String getLogs() {
        return "index";
    }
}
