package com.getjavajob.training.svyaznoy.khaninev.sms.ajax;

import com.getjavajob.training.svyaznoy.khaninev.sms.repository.LogRepository;
import com.getjavajob.training.svyaznoy.khaninev.sms.repository.PageTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by zenet on 20.09.2015.
 */
@Controller
public class AjaxCountController {
    @Autowired
    private LogRepository logRepository;

    @RequestMapping(value = "/count", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    PageTO getLogsJson(@RequestParam(required = false, defaultValue = "10") int itemsCount) {
        return logRepository.getLogsCount(itemsCount);
    }

}
