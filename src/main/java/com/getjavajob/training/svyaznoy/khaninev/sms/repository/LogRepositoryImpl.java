package com.getjavajob.training.svyaznoy.khaninev.sms.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Created by zenet on 18.09.2015.
 */
@Repository
public class LogRepositoryImpl implements LogRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public LogRepositoryImpl(final DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    @Transactional
    public List<LogTO> getLogs(final int pageNumber, final int itemsCount) {
        final String sqlGetLogs = "SELECT * FROM log ORDER BY date DESC LIMIT " + pageNumber * itemsCount + ", " + itemsCount;
        return jdbcTemplate.query(sqlGetLogs, new RowMapper<LogTO>() {
            @Override
            public LogTO mapRow(ResultSet resultSet, int i) throws SQLException {
                LogTO log = new LogTO();
                log.setPhone(resultSet.getString("number"));
                log.setDate(new Date(resultSet.getTimestamp("date").getTime()));
                log.setDeliveryStatus(resultSet.getInt("status"));
                log.setMessage(resultSet.getString("message"));
                return log;
            }
        });
    }

    @Override
    @Transactional
    public void addLog(final String phone, final Date date, final int status, final String message) {
        final String sqlAddLog = "INSERT INTO log (number, date, status, message) VALUES(?, ?, ?, ?)";
        jdbcTemplate.update(sqlAddLog, phone, date, status, message);
    }

    @Override
    @Transactional
    public PageTO getLogsCount(final int itemsCount) {
        final String sqlGetPagesCount = "SELECT COUNT(*) FROM log";
        return jdbcTemplate.query(sqlGetPagesCount, new ResultSetExtractor<PageTO>() {
            @Override
            public PageTO extractData(ResultSet rs) throws SQLException, DataAccessException {
                PageTO page = new PageTO();
                rs.next();
                page.setCount((int) Math.ceil(rs.getInt(1) / itemsCount));
                return page;
            }
        });
    }
}
