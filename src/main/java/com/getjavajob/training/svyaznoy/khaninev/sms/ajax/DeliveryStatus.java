package com.getjavajob.training.svyaznoy.khaninev.sms.ajax;

/**
 * Created by zenet on 20.09.2015.
 */
public class DeliveryStatus {
    private boolean success;

    public DeliveryStatus(boolean status) {
        this.success = status;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean status) {
        this.success = status;
    }
}
