package com.getjavajob.training.svyaznoy.khaninev.sms.repository;

import java.util.Date;

/**
 * Created by zenet on 18.09.2015.
 */
public class LogTO {
    private String phone;
    private Date date;
    private Integer deliveryStatus;
    private String message;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(Integer deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
