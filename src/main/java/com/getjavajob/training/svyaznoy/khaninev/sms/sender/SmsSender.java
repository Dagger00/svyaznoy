package com.getjavajob.training.svyaznoy.khaninev.sms.sender;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * Created by zenet on 18.09.2015.
 */
@Component
public class SmsSender {
    final Logger log = LogManager.getLogger(SmsSender.class);

    final Random random = new Random();

    public boolean sendSms(String phone, String message) {
        boolean success = random.nextBoolean();
        log.info("message sent to {} with status {}:\n{}", phone, success, message);
        return success;
    }
}
