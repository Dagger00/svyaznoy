package com.getjavajob.training.svyaznoy.khaninev.sms.ajax;

import com.getjavajob.training.svyaznoy.khaninev.sms.repository.LogRepository;
import com.getjavajob.training.svyaznoy.khaninev.sms.repository.LogTO;
import com.getjavajob.training.svyaznoy.khaninev.sms.sender.SmsSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * Created by zenet on 18.09.2015.
 */
@Controller
public class AjaxLogsController {

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private SmsSender smsSender;

    @RequestMapping(value = "/logs/{page}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    List<LogTO> getLogsJson(@PathVariable int page, @RequestParam(required = false, defaultValue = "10") int count) {
        return logRepository.getLogs(page, count);
    }

    @RequestMapping(value = "/log", method = RequestMethod.POST)
    public
    @ResponseBody
    DeliveryStatus sendMessage(@RequestParam String phone, @RequestParam String message) {
        final boolean status = smsSender.sendSms(phone, message);
        if (status) {
            logRepository.addLog(phone, new Date(), 1, message);
        } else {
            logRepository.addLog(phone, new Date(), 0, message);
        }
        return new DeliveryStatus(status);
    }
}