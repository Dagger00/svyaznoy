package com.getjavajob.training.svyaznoy.khaninev.sms.repository;

import java.util.Date;
import java.util.List;

/**
 * Created by zenet on 18.09.2015.
 */
public interface LogRepository {
    List<LogTO> getLogs(int pageNumber, int itemsCount);

    void addLog(String phone, Date date, int status, String message);

    PageTO getLogsCount(int itemsCount);
}
