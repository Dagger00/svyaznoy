<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sms Sender</title>
    <link href="${pageContext.servletContext.contextPath}static/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.servletContext.contextPath}static/css/main.css" rel="stylesheet">
</head>
<body>
<div class="container">

    <div class="row">
        <div class="col-md-3">
            <form class="submit">
                <div class="form-group">
                    <label for="phone">phone</label>
                    <input type="text" class="form-control" id="phone" placeholder="phone" required="required">
                </div>
                <div class="form-group">
                    <label for="message">message</label>
                    <textarea class="form-control" id="message" placeholder="message" required="required"></textarea>
                </div>

                <button type="submit" class="btn btn-default">Send</button>
            </form>
            <div class="message-status">

            </div>
        </div>
        <div class="col-md-9">
            <div class="right">
                <div class="panel panel-default">
                    <div class="panel-heading">Sms</div>

                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>
                                Phone
                            </th>
                            <th>
                                Sent Date
                            </th>
                            <th>
                                Sent Status
                            </th>
                            <th>
                                Message
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="page" data-id="" style="display: none;">

        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="pag">

            </div>
        </div>
    </div>
</div>
<script class="template" type="text/template">
    <tr class='{{name }}'>
        <td class="phone">{{phone }}</td>
        <td class="date">{{-date }}</td>
        <td class="status">{{-status }}</td>
        <td class="message">{{-message }}</td>
    </tr>
</script>
<script class="nav-template" type="text/template">
    <ul class="pagination">
        [[ for(var i = 1; i <= pageCount; ++i){ ]]
        [[ if(page === i-1){ ]]
        <li class="active"><a data-id="{{-i }}">{{-i }}</a></li>
        [[ } else { ]]
        <li><a data-id="{{-i }}">{{-i }}</a></li>
        [[ } ]]
        [[ } ]]
    </ul>
</script>
<script>
    var context = "${pageContext.servletContext.contextPath}";
</script>
<script src="${pageContext.servletContext.contextPath}static/js/jquery.js"></script>
<script src="${pageContext.servletContext.contextPath}static/js/moments.js"></script>
<script src="${pageContext.servletContext.contextPath}static/js/underscore.js"></script>
<script src="${pageContext.servletContext.contextPath}static/js/main.js"></script>
</body>
</html>