$(function () {

    var itemsCount = 10;
    moment.locale('ru');
    _.templateSettings = {
        interpolate: /\{\{(.+?)\}\}/g,
        evaluate: /\[\[(.+?)\]\]/g,
        escape: /\{\{-(.+?)\}\}/g
    };

    var templateString = $('.template').html();
    var templateStringNav = $('.nav-template').html();
    var smsTemplate = _.template(templateString);
    var smsTemplateNav = _.template(templateStringNav);

    function getPagesCount() {
        return $.ajax({
            url: context + '/count',
            method: 'GET',
            data: {
                itemsCount: itemsCount
            }
        });
    }


    function renderStatus(status) {
        if (status == 1) {
            return 'Отправлено';
        }
        return 'Не отправлено';
    }

    function renderDate(dateTime) {
        var date = new Date(dateTime);
        return moment(date).format("YYYY-MM-DD HH:mm:ss");
    }

    var getPage = function (page) {
        $.getJSON(context + '/logs/' + page, {count: itemsCount}, function (json) {
            var html = '';
            $.each(json, function (key, value) {
                html += smsTemplate({
                    name: key,
                    phone: value.phone,
                    date: renderDate(value.date),
                    status: renderStatus(value.deliveryStatus),
                    message: value.message
                });
            });
            var tbody = $('tbody').html(html);

            $('.page').data('id', page);

            getPagesCount().done(function (c) {
                var pag = $('.pag');
                var htmlNav = smsTemplateNav({
                    pageCount: c.count,
                    page: page
                });
                pag.html(htmlNav);
            });
        });
    };

    getPage(0);

    $(document).on('click', 'ul li a', function () {
        event.preventDefault();
        getPage($(this).data('id') - 1);
    });

    function isValidPhone(phone) {
        var pattern = new RegExp(/^79\d{9}$/);
        return pattern.test(phone);
    }

    function isValidMessage(message) {
        message = message.trim();
        return message.length > 0;
    }

    $('.submit').submit(function (event) {
        event.preventDefault();
        var phone = event.target[0].value;
        var message = event.target[1].value;
        var status = $('.message-status');
        var inputs = $("input, textarea");
        if (!isValidPhone(phone)) {
            status.text('Неправильно введён телефон');
            return;
        }
        if (!isValidMessage(message)) {
            status.text('Сообщение не может быть пустым');
            return;
        }

        $.ajax({
            url: context + '/log',
            method: 'POST',
            data: {
                phone: phone,
                message: message
            }
        }).done(function (deliveryStatus) {
            getPage($('.page').data('id'));
            inputs.prop('disabled', false);
            if (deliveryStatus.success) {
                status.text('Сообщение успешно отправлено!');
                inputs.val('');
            } else {
                status.text('Произошла ошибка, сообщение не отправлено!');
            }
        }).fail(function () {
            status.text('Server exception');
        });
        inputs.prop('disabled', true);
        status.text('"Отправка сообщения..');
    });
});